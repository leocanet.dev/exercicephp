<?php
// Demarre une session pour un utilisateur
session_start();
//Etablir la connection et la vérifier
$host = 'localhost';
$dbname = 'users';
$username = 'admin';
$password = '132411';

$conn = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);

//Lorsque l'utilsateur clique sur le bouton d'envoi, je déclare mes variables  
if(isset($_POST['envoi'])){
    $pseudo = $_POST['pseudo'];
    $mdp = $_POST['mdp'];
// Je recupere sous forme de tableau,le mot de passe et le pseudo dans la table utilisateurs de ma base de donnée
    $recup_user = $conn->prepare('SELECT * FROM utilisateurs WHERE pseudo = ? AND mdp = ?');
    $recup_user->execute(array($pseudo, $mdp));
// Je verifie si les informations sont présentes dans la table
    if($recup_user->rowCount() > 0){
        $_SESSION['pseudo'] = $pseudo;
        $_SESSION['mdp'] = $mdp;
        $_SESSION['id'] = $recup_user->fetch(['id']);
// Si les identifiants sont correct : l'utilisateur est renvoyé vers protect.php
        header('Location: protect.php');
    }else{
// Si les identifiants sont incorrect : l'utilisateur est renvoyé vers index.html via la balise meta HTML   
        $bye = "Vos identifiants sont incorrect...";
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="refresh" content="3; URL=deco.php"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    <title>Document</title>
</head>
<body>
    <h1><?php echo $bye; ?></h1>
</body>
</html>

